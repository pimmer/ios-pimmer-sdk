// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PimKit",
    defaultLocalization: "en",
    platforms: [.iOS("12.0")],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "PimKit",
            targets: ["PimKit"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
            .binaryTarget(
                name: "PimKit",
                url: "https://sdk.pimmer.app/ios-sdk/PimKit.2.3.206.xcframework.zip",
                checksum: "df670ff05a6f9ad5c3f467a0f9239a45535ff1671053e6200f2470e27d754746"
            )
        ]
)

